# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('newsletter', '0002_auto_20151104_0013'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='signup',
            name='email',
        ),
        migrations.AddField(
            model_name='signup',
            name='username',
            field=models.CharField(default=datetime.datetime(2015, 11, 3, 23, 36, 36, 736000, tzinfo=utc), max_length=100),
            preserve_default=False,
        ),
    ]
