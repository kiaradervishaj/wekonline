# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('newsletter', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='signup',
            name='full_name',
        ),
        migrations.AddField(
            model_name='signup',
            name='password',
            field=models.CharField(default=datetime.datetime(2015, 11, 3, 23, 13, 8, 379000, tzinfo=utc), max_length=50),
            preserve_default=False,
        ),
    ]
