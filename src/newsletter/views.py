from django.shortcuts import render
from .models  import Note
from .forms import SignUpForm, NoteForm
# Create your views here.

def home(request):
	
	notes = Note.objects.all()
	
	title='Welcome'
	signup='Please sign up to start taking notes.'

	form=SignUpForm(request.POST or None)
	form2=NoteForm(request.POST or None)

	context={
		'title': title,
		'signup': signup,
		'form': form,
		'notes': notes,
	}
	

	if form.is_valid():
		form.save()
		context={
			'title': 'Your note is:',
			'form2': form2,
			'notes': notes,
		}
	
	if form2.is_valid():
		form2.save()
		context={
			'title': 'Your note is',
			'form2': form2,
			'notes': notes,
		}

	return render(request, "home.html", context)