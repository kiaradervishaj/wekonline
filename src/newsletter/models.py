from django.db import models

from django import forms
# Create your models here.

class Note(models.Model):
	note=models.CharField(max_length=2000, blank=False)

	def __unicode__ (self):
		return self.note

class SignUp(models.Model):
	username=models.CharField(max_length=100)
	password=models.CharField(max_length=50)
	timestamp=models.DateTimeField(auto_now_add=True, auto_now=False)
	updated=models.DateTimeField(auto_now_add=False, auto_now=True)

	def __unicode__ (self):
		return self.username