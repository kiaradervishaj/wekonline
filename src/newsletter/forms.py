from django import forms

from .models import SignUp, Note

class NoteForm(forms.ModelForm):
	note=forms.CharField()
	class Meta:
		model=Note
		fields=['note']

class SignUpForm(forms.ModelForm):
	class Meta:
		model=SignUp
		widgets={
			'password': forms.PasswordInput(),
		}

		fields=['username', 'password']